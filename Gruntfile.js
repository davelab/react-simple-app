/*global module:false*/
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({

    sass : {
      options : {
          compass: true,
          style: 'expanded'
      },

      css : {
          files: {
              'public/style/style.css': 'scss/main.scss'
          }
      }
  },

      concat: {
        options: {
          separator: ';',
        },
        dist: {
          src: [
                    'node_modules/underscore/underscore.js',
                    'node_modules/classnames/index.js',
                    'node_modules/pubsub-js/src/pubsub.js',
                    'node_modules/react/dist/react-with-addons.js',
                    'node_modules/react-dom/dist/react-dom.js'
                ],
          dest: 'public/lib/react.js',
        },
      },

    watch: {
        css: {
            files: 'scss/**/*.scss',
            tasks: ['sass:css'],
            options: {
              livereload: false,
            }
        }
    },

    babel: {
      options: {
        plugins: ['transform-react-jsx'],
        presets: ['es2015', 'react']
      },
      jsx: {
        files: [{
          expand: true,
          cwd: 'src/js/', // Custom folder
          src: ['*.jsx'],
          dest: 'public/js/', // Custom folder
          ext: '.js'
        }]
      }
    }

  });

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-babel');
  grunt.loadNpmTasks('grunt-contrib-concat');

  // Default task.
  grunt.registerTask('start', ['concat:dist']);
  grunt.registerTask('css', ['sass:css']);
  grunt.registerTask('jsx', ['babel:jsx']);

};
