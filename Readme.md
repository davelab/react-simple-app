This is test is build using [React.js](https://facebook.github.io/react/)

## How to run the project

- run `$ npm install`
- run `$ node server.js`

## Grunt tasks

- compile JS dependencies `$ grunt start`
- compile sass: `$ grunt css`
- compile jsx application file `grunt jsx`
