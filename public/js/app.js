'use strict';

function randomDatas() {
    var all = ['Limit X reached', '162 updates', 'Monthly resolution'];
    var newArr = _.sample(all, Math.floor(Math.random() * 3 + 1));
    return newArr;
}

var data = [{ id: 1, name: "Metric name 1", info: randomDatas() }, { id: 2, name: "Metric name 2", info: randomDatas() }, { id: 3, name: "Metric name 3", info: randomDatas() }, { id: 4, name: "Metric name 4", info: randomDatas() }, { id: 5, name: "Metric name 5", info: randomDatas() }];

var ENTER_KEY = 13,
    ESC_KEY = 27,
    E_KEY = 69,
    N_KEY = 78;

var App = React.createClass({
    displayName: 'App',
    getInitialState: function getInitialState() {
        return {
            data: this.props.data
        };
    },
    handleKeyDown: function handleKeyDown(e) {
        console.log(e);
        if (e.which === N_KEY && e.ctrlKey) {
            this.addMetric(e);
        }
    },
    componentDidMount: function componentDidMount() {
        document.addEventListener('keydown', this.handleKeyDown, true);
    },
    addMetric: function addMetric(e) {
        e.preventDefault();
        var newNode = { id: this.state.data.length + 1, name: 'new metric', info: randomDatas() };

        this.setState({ data: this.state.data.concat([newNode]) }, function () {
            PubSub.publish('newMeticCard', newNode);
        });
    },
    deleteMetric: function deleteMetric(e) {
        var nodeId = parseInt(e.target.parentElement.value, 10);
        this.setState(function (state) {
            state.data = _.without(state.data, _.findWhere(state.data, { id: nodeId }));
            return { items: state.data };
        });
    },
    render: function render() {
        return React.createElement(
            'div',
            null,
            React.createElement(Header, null),
            React.createElement(MetricsList, { data: this.state.data,
                deleteMetric: this.deleteMetric,
                addMetric: this.addMetric }),
            React.createElement(
                'footer',
                { className: 'text-center mt' },
                'Press ',
                React.createElement(
                    'code',
                    null,
                    'CTRL+N'
                ),
                ' for add new metric ',
                React.createElement('br', null),
                'Press ',
                React.createElement(
                    'code',
                    null,
                    'CTRL+E'
                ),
                ' for edit metrics'
            )
        );
    }
});

var Header = React.createClass({
    displayName: 'Header',
    render: function render() {
        return React.createElement(
            'header',
            null,
            React.createElement('div', { className: 'logo' }),
            React.createElement(
                'div',
                { className: 'flex-container mt' },
                React.createElement(
                    'div',
                    { className: 'ellipsisfy mr' },
                    'averyverylargeemailhere@company.com, averyverylargeemailhere@company.com, averyverylargeemailhere@company.com acco'
                ),
                React.createElement(
                    'div',
                    null,
                    React.createElement(EditModeButton, null)
                )
            )
        );
    }
});

var EditModeButton = React.createClass({
    displayName: 'EditModeButton',
    getInitialState: function getInitialState() {
        return { isEditMode: false };
    },
    toggleEditMode: function toggleEditMode() {
        var newEditState = !this.state.isEditMode;
        this.setState({ isEditMode: newEditState });
        PubSub.publish('isEdit', newEditState);
    },
    handleKeyDown: function handleKeyDown(e) {
        console.log(e);
        if (e.which === E_KEY && e.ctrlKey) {
            this.toggleEditMode();
        }
    },
    componentDidMount: function componentDidMount() {
        document.addEventListener('keydown', this.handleKeyDown, true);
    },
    render: function render() {
        var btnClass = classNames({
            'btn': true,
            'btn-flat-purple': !this.state.isEditMode,
            'btn-flat-pink': this.state.isEditMode
        });
        var label = this.state.isEditMode ? 'EXIT EDIT MODE' : React.createElement('i', { className: 'fa fa-pencil' });
        return React.createElement(
            'button',
            { type: 'button', className: btnClass, onClick: this.toggleEditMode },
            label
        );
    }
});

var MetricsList = React.createClass({
    displayName: 'MetricsList',
    getInitialState: function getInitialState() {
        return {
            data: this.props.data,
            isMetricEdit: false
        };
    },
    save: function save(itemToUpdate, val) {
        this.setState(function (state) {
            var match = _.find(state.data, function (item) {
                return item.id === itemToUpdate.id;
            });
            if (match) {
                match.name = val;
            }
        });
    },
    componentDidMount: function componentDidMount() {
        this.pubsub_token_new = PubSub.subscribe('newMeticCard', function (topic, newNodeObj) {
            this.setState({ data: this.props.data });
        }.bind(this));
    },
    render: function render() {
        var _this = this;

        var metricNodes = this.props.data.map(function (metric) {
            return React.createElement(MetricItem, { metric: metric,
                key: metric.id,
                ref: metric.id,
                deleteMetric: _this.props.deleteMetric,
                onSave: _this.save.bind(_this, metric)
            });
        });
        return React.createElement(
            'div',
            { className: 'flex-grid-container mt--l' },
            metricNodes,
            React.createElement(AddMetricCard, { addMetric: this.props.addMetric })
        );
    }
});

var MetricItem = React.createClass({
    displayName: 'MetricItem',
    getInitialState: function getInitialState() {
        return {
            isEdit: false,
            metric: '',
            isMetricEdit: false
        };
    },
    componentWillMount: function componentWillMount() {
        this.pubsub_token_is_edit = PubSub.subscribe('isEdit', function (topic, isEdit) {
            this.setState({ isEdit: isEdit });
        }.bind(this));
    },
    componentDidMount: function componentDidMount() {
        this.pubsub_token_new = PubSub.subscribe('newMeticCard', function (topic, newNodeObj) {
            if (_.has(this.refs, newNodeObj.id)) {
                console.log(ReactDOM.findDOMNode(this.refs[newNodeObj.id + '_title']));
                ReactDOM.findDOMNode(this.refs[newNodeObj.id + '_title']).click();
            }
        }.bind(this));
    },
    componentWillUnmount: function componentWillUnmount() {
        PubSub.unsubscribe(this.pubsub_token_new);
        PubSub.unsubscribe(this.pubsub_token_is_edit);
    },
    onChange: function onChange(e) {
        this.setState({ metric: e.target.value });
    },
    toggleEditMetricName: function toggleEditMetricName() {
        var newEditMetric = !this.state.isMetricEdit;
        this.setState({ isMetricEdit: newEditMetric }, function () {
            if (this.state.isMetricEdit) {

                var inputNode = ReactDOM.findDOMNode(this.refs.metric_input);

                inputNode.focus();
                inputNode.select();
            }
        });
    },
    onBlur: function onBlur(defaultMetricName) {
        //document.getElementById("metric-name").value(defaultMetricName);
        if (this.state.isMetricEdit) {
            this.setState({ isMetricEdit: false });
        }
    },
    handleKeyDown: function handleKeyDown(event) {
        if (event.which === ENTER_KEY) {
            this.handleSubmit(event);
        } else if (event.which === ESC_KEY) {
            this.onBlur();
        }
    },
    handleSubmit: function handleSubmit(event) {
        var val = this.state.metric.trim();
        if (val) {
            this.props.onSave(val);
        }
        if (this.state.isMetricEdit) {
            this.setState({ isMetricEdit: false });
        }
    },
    render: function render() {
        var btnClass = classNames({
            'btn-circle': true,
            'card__delete': true,
            'show': this.state.isEdit
        });
        var inputClass = classNames({
            'show': this.state.isMetricEdit
        });
        var cardTitleClass = classNames({
            'card__title': true,
            'hide': this.state.isMetricEdit
        });

        return React.createElement(
            'div',
            { className: 'card', ref: this.props.metric.id },
            React.createElement(
                'div',
                { className: 'card__container' },
                React.createElement(
                    'p',
                    { className: cardTitleClass, ref: this.props.metric.id + '_title', onClick: this.toggleEditMetricName },
                    this.props.metric.name,
                    React.createElement(
                        'button',
                        { className: 'edit' },
                        React.createElement('i', { className: 'fa fa-pencil' })
                    )
                ),
                React.createElement('input', { type: 'text', ref: 'metric_input',
                    className: inputClass,
                    onKeyDown: this.handleKeyDown,
                    onBlur: this.onBlur,
                    onChange: this.onChange,
                    defaultValue: this.props.metric.name }),
                React.createElement(
                    'div',
                    { className: 'card__description' },
                    React.createElement(
                        'div',
                        null,
                        this.props.metric.info.map(function (item) {
                            return React.createElement(
                                'p',
                                null,
                                ' ',
                                item,
                                '  '
                            );
                        })
                    )
                ),
                React.createElement(
                    'button',
                    { className: btnClass, onClick: this.props.deleteMetric, value: this.props.metric.id },
                    React.createElement('i', { className: 'fa fa-close' })
                )
            )
        );
    }
});

var AddMetricCard = React.createClass({
    displayName: 'AddMetricCard',
    getInitialState: function getInitialState() {
        return { isEdit: false };
    },
    componentWillMount: function componentWillMount() {
        this.pubsub_token = PubSub.subscribe('isEdit', function (topic, isEdit) {
            this.setState({ isEdit: isEdit });
        }.bind(this));
    },
    render: function render() {
        var AddCardClass = classNames({
            'card': true,
            'card--new': true,
            'hide': this.state.isEdit
        });
        return React.createElement(
            'div',
            { className: AddCardClass, onClick: this.props.addMetric },
            React.createElement(
                'div',
                { className: 'mr--l' },
                React.createElement('i', { className: 'fa fa-plus' })
            ),
            React.createElement(
                'div',
                null,
                'CREATE NEW METRIC'
            )
        );
    }
});

ReactDOM.render(React.createElement(App, { data: data }), document.getElementById('app'));
