/**
 * Application main file
 *
 * this file is the entire frontend application test
 */


// this function gets a shuffle set of metric info
function randomDatas() {
    var all = ['Limit X reached', '162 updates', 'Monthly resolution'];
    var newArr = _.sample(all, Math.floor((Math.random() * 3) + 1));
    return newArr;
}

// fixture data
var data = [
    {id: 1, name: "Metric name 1", info: randomDatas()},
    {id: 2, name: "Metric name 2", info: randomDatas()},
    {id: 3, name: "Metric name 3", info: randomDatas()},
    {id: 4, name: "Metric name 4", info: randomDatas()},
    {id: 5, name: "Metric name 5", info: randomDatas()}
];

// Main keycode used in the application at event keydown
var ENTER_KEY   = 13,
    ESC_KEY     = 27,
    E_KEY       = 69,
    N_KEY       = 78;

// Application main component
var App = React.createClass({
    getInitialState() {
        return {
            data: this.props.data
        }
    },
    handleKeyDown(e) {
        console.log(e);
        if (e.which === N_KEY && e.ctrlKey) {
            // create new metric card by keydow combination ctrl+n
            this.addMetric(e);
        }
    },
    componentDidMount() {
        // add event listener to provide a ctrl+n action
        document.addEventListener('keydown', this.handleKeyDown, true);
    },
    addMetric(e) {
        e.preventDefault();
        // add a new static object to the app
        var newNode = {id: this.state.data.length + 1, name: 'new metric', info: randomDatas()};

        this.setState({data: this.state.data.concat([newNode])}, function() {
            // lunch an event object as been added to the app
            PubSub.publish('newMeticCard', newNode);
        });

    },
    deleteMetric(e) {
        var nodeId = parseInt(e.target.parentElement.value, 10);
        //remove the object from the state data
        this.setState(state => {
             state.data = _.without(state.data, _.findWhere(state.data, {id: nodeId}));
            return {items: state.data};
        });
    },
    render () {
        return (
            <div>
                <Header />
                <MetricsList data={this.state.data}
                            deleteMetric={this.deleteMetric}
                            addMetric={this.addMetric} />
                        <footer className="center mt">
                            Press <code>CTRL+N</code> for add new metric <br />
                            Press <code>CTRL+E</code> for edit metrics
                        </footer>
            </div>
        )
    }
});

var Header = React.createClass({
    render () {
        return (
            <header>
                <div className="logo"></div>
                <div className="flex-container mt">
                    <div className="ellipsisfy mr">
                        averyverylargeemailhere@company.com, averyverylargeemailhere@company.com, averyverylargeemailhere@company.com acco
                    </div>
                    <div>
                        <EditModeButton />
                    </div>
                </div>
            </header>
        )
    }
});

var EditModeButton = React.createClass({
    //set the inital state of the app (not in edit mode)
    getInitialState() {
       return { isEditMode: false };
    },
    //handle the state of edit mode by switching the state it self
    //and lunching an event once the state is change,
    //so the cards will change also their state in order to be deleted
    toggleEditMode() {
        var newEditState = !this.state.isEditMode
        this.setState({ isEditMode: newEditState});
        PubSub.publish('isEdit', newEditState);
    },
    //allow to switch the edit mode by CTRL+E
    handleKeyDown(e) {
        console.log(e);
        if (e.which === E_KEY && e.ctrlKey) {
            this.toggleEditMode();
        }
    },
    componentDidMount() {
        document.addEventListener('keydown', this.handleKeyDown, true);
    },
    render () {
        var btnClass = classNames({
          'btn': true,
          'btn-flat-purple': !this.state.isEditMode,
          'btn-flat-pink': this.state.isEditMode
        });
        var label = this.state.isEditMode ? 'EXIT EDIT MODE' : <i className="fa fa-pencil"></i>;
        return (
            <button type="button" className={btnClass} onClick={this.toggleEditMode}>
                    {label}
            </button>
        )
    }
});

var MetricsList = React.createClass({
    getInitialState() {
        return {
            data: this.props.data,
            isMetricEdit: false
        }
    },

    //handle the save of a metric name edit.
    save(itemToUpdate, val) {
        this.setState(state => {
            //find the state data with the same id and change the value of the name property
            var match = _.find(state.data, function(item) { return item.id === itemToUpdate.id })
            if (match) {
                match.name = val;
            }
        });
    },
    componentDidMount() {
        // update the list state data once the new card is added
        this.pubsub_token_new = PubSub.subscribe('newMeticCard', function(topic, newNodeObj) {
                                this.setState({data: this.props.data});
                            }.bind(this));
    },
    render () {
        var metricNodes = this.props.data.map((metric) =>
                            <MetricItem metric={metric}
                                        key={metric.id}
                                        ref={metric.id}
                                        deleteMetric={this.props.deleteMetric}
                                        onSave={this.save.bind(this, metric)}
                                        />
                        );
        return (
            <div className="flex-grid-container mt--l">
                {metricNodes}
                <AddMetricCard addMetric={this.props.addMetric} />
            </div>
        )
    }
});

var MetricItem = React.createClass({
    getInitialState() {
        return {
            isEdit: false,
            metric: '',
            isMetricEdit: false
         };
    },
    // once the event of edit is fired by a click on the edit button or from CTRL+E
    // a button with x will appeard in all the cards
    componentWillMount() {
        this.pubsub_token_is_edit = PubSub.subscribe('isEdit', function(topic, isEdit) {
                                this.setState({ isEdit: isEdit });
                            }.bind(this));
    },
    componentDidMount() {
        //listen to the new card event and place a change the edit mode for the card and focus on the input field
        this.pubsub_token_new = PubSub.subscribe('newMeticCard', function(topic, newNodeObj) {
                                if (_.has(this.refs, newNodeObj.id)) {
                                    console.log(ReactDOM.findDOMNode(this.refs[newNodeObj.id + '_title']));
                                    ReactDOM.findDOMNode(this.refs[newNodeObj.id + '_title']).click();
                                }
                            }.bind(this));
    },
    componentWillUnmount() {
        //unsubscribe events from a deleted metric card's component
        PubSub.unsubscribe(this.pubsub_token_new);
        PubSub.unsubscribe(this.pubsub_token_is_edit);
    },
    onChange(e) {
       this.setState({ metric: e.target.value });
    },
    // this method allow to edit the name of a card setting the focus and select all the input's value
    // and hiding a <p>
    toggleEditMetricName() {
        var newEditMetric = !this.state.isMetricEdit
        this.setState({ isMetricEdit: newEditMetric}, function() {
            if (this.state.isMetricEdit) {

                var inputNode = ReactDOM.findDOMNode(this.refs.metric_input);

                inputNode.focus();
                inputNode.select();
            }
        });
    },
    onBlur(defaultMetricName) {
        //if a blur is fired from a input field the metric card name won't change
        //and then put a default status the card
        if (this.state.isMetricEdit) {
            this.setState({isMetricEdit: false});
        }
    },
    handleKeyDown(event) {
        //this allow to ESC to act like a simple blur or
        //to press enter and save the new status of edited card.
        if (event.which === ENTER_KEY) {
            this.handleSubmit(event);
        } else if (event.which === ESC_KEY) {
            this.onBlur();
        }
    },
    //call a functio after the enter is pressed that act like a form subit,
    //saving the new metric name
    handleSubmit(event) {
        var val = this.state.metric.trim();
			if (val) {
                this.props.onSave(val)
            }
        if (this.state.isMetricEdit) {
            this.setState({isMetricEdit: false});
        }
    },
    render () {
        var btnClass = classNames({
          'btn-circle': true,
          'card__delete': true,
          'show': this.state.isEdit
        });
        var inputClass = classNames({
            'show': this.state.isMetricEdit
        });
        var cardTitleClass= classNames({
            'card__title' : true,
            'hide': this.state.isMetricEdit
        });

        return (
            <div className="card" ref={this.props.metric.id}>
                <div className="card__container">
                    <p className={cardTitleClass} ref={this.props.metric.id+'_title'} onClick={this.toggleEditMetricName}>
                        {this.props.metric.name}
                        <button className="edit">
                            <i className="fa fa-pencil"></i>
                        </button>
                    </p>
                    <input type="text" ref="metric_input"
                        className={inputClass}
                        onKeyDown={this.handleKeyDown}
                        onBlur={this.onBlur}
                        onChange={this.onChange}
                        defaultValue={this.props.metric.name} />

                    <div className="card__description">
                        <div>
                            {
                                this.props.metric.info.map(function(item) {
                                    return <p> {item}  </p>
                                })
                            }
                        </div>
                    </div>

                    <button className={btnClass} onClick={this.props.deleteMetric} value={this.props.metric.id}>
                        <i className="fa fa-close"></i>
                    </button>
                </div>
            </div>
        )
    }
});

var AddMetricCard = React.createClass({
    getInitialState() {
        return { isEdit: false };
    },

    componentWillMount() {
        this.pubsub_token = PubSub.subscribe('isEdit', function(topic, isEdit) {
                                this.setState({ isEdit: isEdit });
                            }.bind(this));
    },
    render () {
        var AddCardClass = classNames({
          'card': true,
          'card--new': true,
          'hide': this.state.isEdit
        });
        return (
            <div className={AddCardClass} onClick={this.props.addMetric} >
                    <div className="mr--l">
                        <i className="fa fa-plus"></i>
                    </div>
                    <div>
                        CREATE NEW METRIC
                    </div>
            </div>
        )
    }
});

ReactDOM.render(
  <App data={data} />,
  document.getElementById('app')
);
